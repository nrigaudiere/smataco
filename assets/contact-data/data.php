<?php

$output_dir = "../uploads/";

include 'ChromePhp.php';


// $data = $_POST['image'];
// $fileName = $_POST['name'];
// $serverFile = time().$fileName;
// $fp = fopen('../uploads/'.$serverFile,'w'); //Prepends timestamp to prevent overwriting
// fwrite($fp, $data);
// fclose($fp);
// 
// if(isset($_FILES["image"]))
// {
// 	$ret = array();

// 	$error =$_FILES["myfile"]["error"];
// 	//You need to handle  both cases
// 	//If Any browser does not support serializing of multiple files using FormData() 
// 	if(!is_array($_FILES["myfile"]["name"])) //single file
// 	{
//  	 	$fileName = $_FILES["myfile"]["name"];
//  		move_uploaded_file($_FILES["myfile"]["tmp_name"],$output_dir.$fileName);
//     	$ret[]= $fileName;
// 	}
// 	else  //Multiple files, file[]
// 	{
// 	  $fileCount = count($_FILES["myfile"]["name"]);
// 	  for($i=0; $i < $fileCount; $i++)
// 	  {
// 	  	$fileName = $_FILES["myfile"]["name"][$i];
// 		move_uploaded_file($_FILES["myfile"]["tmp_name"][$i],$output_dir.$fileName);
// 	  	$ret[]= $fileName;
// 	  }
	
// 	}
//     echo json_encode($ret);
//  }

if(isset($_POST['image']) && isset($_POST['name']) && isset($_POST['email']) && isset($_POST['info']) && isset($_POST['city']) && isset($_POST['country'])) {
    if(($_POST['image'] !== '') && ($_POST['name'] !== '') && ($_POST['email'] !== '') && ($_POST['info'] !== '') && ($_POST['city'] !== '') && ($_POST['country'] !== '')) {
        $reponse = 'true';

        // Change filename
        $filename    = basename($_POST['image']);
		$extension   = pathinfo($filename, PATHINFO_EXTENSION);
		$name 	     = strtolower(preg_replace('/\s+/', '', $_POST['name']).trim());
		$country     = strtolower($_POST['country'].trim());
		$newFilename = $name."_".$country;

		// ChromePhp::log("../uploads/".$filename.".".$extension);

		rename("../uploads/".$filename, "../uploads/".$newFilename.".".$extension);

		//Add data to file
		$file= "data.csv";

        $data = $_POST['name'].",".$_POST['email'].",".$_POST['info'].",".$_POST['city'].",".$_POST['country'].",".$newFilename.".".$extension."\n";
        file_put_contents($file, $data, FILE_APPEND);


    } else {
        $reponse = 'false';
    }
} else {
    $reponse = 'We didn\'t get all the fields';
}
 
echo $reponse;
?>